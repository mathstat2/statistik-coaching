# Statistik Coaching


## Verweise
Das Skript bezieht sich auf verschiedene Quellen, deren inhaltliche Aufarbeitung und Darstellung der Thematik, als Grundlage und Bezugspunkt beim Erstellen dieses Skriptes dienten. Hierbei sei vor allem auf folgende Quellen verwiesen, wobei es sich bei allen Quellen um Free-and-Open-Source Lehrinhalte der jeweiligen Autoren handelt:

*  [Einführung in R Grundlagen] von [René Kruse](https://gitlab.gwdg.de/kruse44/intro2r/).
* [Advanced R](http://adv-r.had.co.nz/) von [Hadley Wickham](http://hadley.nz/)
* [Merely Useful: Novice R](https://merely-useful.github.io/r/index.html) von [Madeleine Bonsma-Fisher et al.]()
* [R for Data Science](#https://r4ds.had.co.nz/) von [Hadley Wickham](http://hadley.nz/) und [Garrett Grolemund](https://twitter.com/statgarrett?lang=de)
* [Hands-On Programming with R](#https://rstudio-education.github.io/hopr/) von [Garrett Grolemund](https://twitter.com/statgarrett?lang=de)
* [Fundamentals of Data Visualization](#https://serialmentor.com/dataviz/) von [Claus O. Wilke](https://github.com/clauswilke)
* [YaRrr! The Pirate’s Guide to R](#https://bookdown.org/ndphillips/YaRrr/) von [Nathaniel D. Phillips](https://ndphillips.github.io/index.html)
*  Vorlesungsfolien des Statistik-Master Kurses "Introduction to Statistical Programming" und "Lineare Modelle" von [Paul Wiemann](https://www.uni-goettingen.de/de/525900.html).
